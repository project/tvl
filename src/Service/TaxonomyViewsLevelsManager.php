<?php

namespace Drupal\tvl\Service;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Default implementation of TaxonomyViewsLevelsManagerInterface
 *
 * The manager will inspect the configuration of the passed TermInterface
 * object
 * and determine which view will be injected as the page output.
 *
 * At a later point, it would be great to support adherence to the Views
 * permission settings, there is an outstanding patch and issue for that.
 */
class TaxonomyViewsLevelsManager implements TaxonomyViewsLevelsManagerInterface, ContainerAwareInterface {

  use ContainerAwareTrait;

  /**
   * @var \Drupal\Core\ConfigFactory;
   */
  private $config;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $requestStack;

  /**
   * TaxonomyViewsLevelsManager constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   */
  public function __construct(ConfigFactory $config, EntityTypeManagerInterface $entity_type_manager, RequestStack $requestStack) {
    $this->config = $config;
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $config = $container->get('config.factory');
    $entity_type_manager = $container->get('entity_type.manager');
    $requestStack = $container->get('request_stack');
    return new static($config, $entity_type_manager, $requestStack);
  }

  /**
   * Get the taxonomy view integrator settings for this term entity.
   *
   * @param \Drupal\taxonomy\TermInterface $taxonomy_term
   *
   * @return \Drupal\Core\Config\Config
   */
  public function getTermConfigSettings(TermInterface $taxonomy_term) {
    return $this->config->get('tvl.taxonomy_term.' . $taxonomy_term->id());
  }

  /**
   * Get the taxonomy view integrator settings for this terms vocabulary entity.
   *
   * @param \Drupal\taxonomy\TermInterface $taxonomy_term
   *
   * @return \Drupal\Core\Config\Config
   */
  public function getVocabularyConfigSettings(TermInterface $taxonomy_term) {
    return $this->config->get('tvl.taxonomy_vocabulary.' . $taxonomy_term->bundle());
  }

  /**
   * Wrapper method for obtaining parents of a given taxonomy term.
   *
   * @param \Drupal\taxonomy\TermInterface $taxonomy_term
   *
   * @return array
   */
  public function getTermParents(TermInterface $taxonomy_term) {
    return $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadAllParents($taxonomy_term->id());
  }

  /**
   * Return an array of arguments from the URI.
   * It is assumed tha URI will be taxonomy/term/{taxonomy_term}, so anything
   * after that will be returned.
   *
   * @return array
   */
  public function getRequestUriArguments() {
    return array_slice(explode('/', $this->requestStack->getCurrentRequest()
      ->getRequestUri()), 3);
  }

  /**
   * {@inheritdoc}
   */
  public function getTaxonomyTermView(TermInterface $taxonomy_term) {
    $view_info = $this->getTaxonomyTermViewAndDisplayId($taxonomy_term);

    $config = $this->getTermConfigSettings($taxonomy_term);
    $view_arguments = [$taxonomy_term->id()];
    // if the option to pass all args to views is enabled, pass them on
    if ($config->get('pass_arguments')) {
      $view_arguments += $this->getRequestUriArguments();
    }

    return Views::getView($view_info['view_id'])->executeDisplay($view_info['display_id'], $view_arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function getTaxonomyTermViewAndDisplayId(TermInterface $taxonomy_term) {

    //get value from cache
    $cid = 'tvl_view_diplayid:' . $taxonomy_term->id();
    if ($cache = \Drupal::cache()->get($cid)) {
      return $cache->data;
    }

    // current taxonomy term
    $current_level_type = $this->get_current_level_type($taxonomy_term);

    $config_view = $this->get_correct_view_config($taxonomy_term, $current_level_type);
    if (empty($config_view['view_id']) || empty($config_view['display_id'])) {
      // check parent terms for settings
      // if parent is enabled and has 'children inherit settings' is checked off
      foreach ($this->getTermParents($taxonomy_term) as $parent) {
        // skip the current term, it is returned by loadAllParents
        if ($taxonomy_term->id() === $parent->id()) {
          continue;
        }

        $config_view = $this->get_correct_view_config($parent, $current_level_type,TRUE);
        if (!empty($config_view['view_id']) && !empty($config_view['display_id'])) {
          break;
        }
      }

      if (empty($config_view['view_id']) || empty($config_view['display_id'])) {
        // check vocab for settings
        $config = $this->getVocabularyConfigSettings($taxonomy_term);
        if ($config->get('enable_override') && $config->get('inherit_settings')) {
          $current_level_type = $this->get_current_level_type($taxonomy_term);
          $config_view = [
            'view_id' => $config->get('view_' . $current_level_type),
            'display_id' => $config->get('view_' . $current_level_type . '_display'),
          ];
        }
      }

    }

    if (!empty($config_view['view_id']) && !empty($config_view['display_id'])) {
      $data = $config_view;
    }
    else{
      // if we have no matches, we are going to call the default term view that ships with core Drupal
      $data = [
        'view_id' => 'taxonomy_term',
        'display_id' => 'page_1',
      ];
    }

    //add cache
    \Drupal::cache()->set($cid, $data, Cache::PERMANENT, $taxonomy_term->getCacheTags());

    return $data;

  }

  /**
   * Return view_id name and display_id for this term entity.
   *
   * @param \Drupal\taxonomy\TermInterface $taxonomy_term
   * @param string $current_level_type
   * @param boolean $check_inherit_settings
   *
   * @return array
   */
  function get_correct_view_config($taxonomy_term, $current_level_type, $check_inherit_settings=FALSE){
    $config = $this->getTermConfigSettings($taxonomy_term);
    if ($config->get('enable_override')) {
      if(($check_inherit_settings == TRUE && $config->get('inherit_settings')) || $check_inherit_settings == FALSE){
        return [
          'view_id' => $config->get('view_' . $current_level_type),
          'display_id' => $config->get('view_' . $current_level_type . '_display'),
        ];
      }
    }
    return [
      'view_id' => '',
      'display_id' => '',
    ];
  }

  /**
   * Return the level type views for this term entity.
   *
   * @param \Drupal\taxonomy\TermInterface $taxonomy_term
   *
   * @return string
   */
  function get_current_level_type($taxonomy_term){
    $vid = $taxonomy_term->bundle();
    $tid = $taxonomy_term->id();

    /** @var \Drupal\taxonomy\TermStorageInterface $taxonomy_term_storage */
    $taxonomy_term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

    $term_parents = $taxonomy_term_storage->loadParents($tid);
    // is first parent
    if(empty($term_parents)){
      return 'first_parent';
    }
    else{
      $term_childrens = $taxonomy_term_storage->loadTree($vid, $tid, NULL, FALSE);
      // is children and parent
      if(!empty($term_childrens)){
        return 'default';
      }
      // is last children
      else{
        return 'last_child';
      }
    }
  }

}
