<?php

namespace Drupal\tvl\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\tvl\Service\TaxonomyViewsLevelsManagerInterface;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\Cache;

class TaxonomyViewsLevelsTermPageController extends ControllerBase {

  /**
   * @var \Drupal\tvl\Service\TaxonomyViewsLevelsManager
   */
  private $term_display_manager;

  /**
   * TaxonomyViewsLevelsTermPageController constructor.
   * @param \Drupal\tvl\Service\TaxonomyViewsLevelsManagerInterface $term_display_manager
   */
  public function __construct(TaxonomyViewsLevelsManagerInterface $term_display_manager) {
    $this->term_display_manager = $term_display_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $term_display_manager = $container->get('tvl.tvl_manager');
    return new static($term_display_manager);
  }

  /**
   * Returns a render array from a View based on the taxonomy view integrator configuration settings for the requested term entity.
   * @param \Drupal\taxonomy\TermInterface $taxonomy_term
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function render(TermInterface $taxonomy_term) {
    $data = $this->term_display_manager->getTaxonomyTermView($taxonomy_term);
    return $data;
  }
}
